import requests

# Call the GitLab Projects API to get information about the project
url = f'https://api.sandbox.odinfusion.com/CrmModule/v1.0/db/Lead/6ba70b65-8f8d-4cbd-8ea6-f2b4a56fb7c5?withLinks=true'
headers = {'Authorization': f'Bearer {ODIN_API_TOKEN}'}

try:
    # Send a GET request to the API
    response = requests.get(url, headers=headers)
    response.raise_for_status()  # Raise an exception if the response status code indicates an error

    # Print the response from the API
    print(response.json())

except requests.exceptions.HTTPError as error:
    print(f'An HTTP error occurred: {error}')
    
except requests.exceptions.RequestException as error:
    print(f'An error occurred: {error}')
