import requests

# Replace these with your GitLab credentials and project ID
gitlab_token = 'glpat-6pRoQ9gzsP-jbcZ9Z_pP'
gitlab_project_id = '45016707'

# Call the GitLab Projects API to get information about the project
url = f'https://gitlab.com/api/v4/projects/{gitlab_project_id}'
headers = {'Authorization': f'Bearer {gitlab_token}'}

try:
    # Send a GET request to the API
    response = requests.get(url, headers=headers)
    response.raise_for_status()  # Raise an exception if the response status code indicates an error

    # Print the response from the API
    print(response.text)

except requests.exceptions.HTTPError as error:
    print(f'An HTTP error occurred: {error}')
    
except requests.exceptions.RequestException as error:
    print(f'An error occurred: {error}')
